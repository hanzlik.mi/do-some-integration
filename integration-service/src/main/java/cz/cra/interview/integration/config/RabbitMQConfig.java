package cz.cra.interview.integration.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Michal Hanzlik
 */
@ConfigurationProperties("cra.integration.rabbitmq")
public record RabbitMQConfig (String host, int port, String username, String password, String exchange, String queue, String binding) {
}


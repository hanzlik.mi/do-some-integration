package cz.cra.interview.integration.transformer;

import cz.cra.interview.integration.model.AccountDTO;
import cz.cra.interview.integration.model.CommonModel;
import org.springframework.stereotype.Component;

@Component
public class AccountToCommonModelTransformer {
    final String OPERATION = "PostAccount";
    public CommonModel transform(AccountDTO account){
        return new CommonModel(OPERATION, account);
    }
}

package cz.cra.interview.integration.routes;

import cz.cra.interview.integration.config.RabbitMQConfig;
import cz.cra.interview.integration.transformer.AccountToCommonModelTransformer;
import cz.cra.interview.integration.model.AccountDTO;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class RestControllerRouter extends RouteBuilder {

    AccountToCommonModelTransformer accountToCommonModelTransformer;
    RabbitMQConfig rabbitMQConfig;
    public RestControllerRouter(AccountToCommonModelTransformer accountToCommonModelTransformer, RabbitMQConfig rabbitMQConfig) {
        this.accountToCommonModelTransformer = accountToCommonModelTransformer;
        this.rabbitMQConfig = rabbitMQConfig;
    }

    @Override
    public void configure() throws Exception {
        restConfiguration()
                .component("servlet")
                .bindingMode(RestBindingMode.auto);

        from("rest:post:CRMEvent/Account")
                .process(this::return200) // return 200 (no response body)
                .to("direct:next-route");

        from("direct:next-route")
                .log("Method: ${headers.CamelHttpMethod}, url: ${headers.CamelHttpUrl}, body: ${body}") // Log received http request (method, url, body)
                .unmarshal(new JacksonDataFormat(AccountDTO.class))
                .bean(accountToCommonModelTransformer, "transform") // Map request body to CommonModel  + Set CommonModel.operation = PostAccount
                .log("CommonModel: ${body}") // Log prepared CommonModel
                .marshal(new JacksonDataFormat()) // marshal CommonModel to JSON
                .log("CommonModel in JSON: ${body}")

                .to("spring-rabbitmq:"+rabbitMQConfig.exchange()+"?queues="+rabbitMQConfig.queue()+"&disableReplyTo=true") // store message in rabbitmq
                .setBody(simple("")) // set response body to empty
                .end();
    }

    private void return200(Exchange exchange) {
        exchange.getMessage().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.OK.value());
    }

}

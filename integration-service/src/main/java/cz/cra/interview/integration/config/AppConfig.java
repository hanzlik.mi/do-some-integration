package cz.cra.interview.integration.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    CachingConnectionFactory rabbitConnectionFactory(RabbitMQConfig rabbitMQConfig){

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(rabbitMQConfig.host());
        connectionFactory.setPort(rabbitMQConfig.port());
        connectionFactory.setUsername(rabbitMQConfig.username());
        connectionFactory.setPassword(rabbitMQConfig.password());

        return connectionFactory;
    }
}

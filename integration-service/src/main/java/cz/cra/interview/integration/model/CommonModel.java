package cz.cra.interview.integration.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CommonModel {

    private String operation;

    private IntegrationApiModel apiModel;


}


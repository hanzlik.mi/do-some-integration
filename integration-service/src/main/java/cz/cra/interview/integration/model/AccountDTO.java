package cz.cra.interview.integration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.util.Date;

/**
 * JSON Example:
 * {
 *   "id": 12345,
 *   "firstname": "John",
 *   "lastname": "Doe",
 *   "updatedAt": "2023-02-14 10:38:00"
 * }
 */
@Data
@Builder
@Jacksonized
public class AccountDTO extends IntegrationApiModel {

    private Long id;

    private String firstname;

    private String lastname;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}


package cz.cra.interview.integration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * JSON Example:
 * {
 *   "id": 12345,
 *   "name": "John Doe",
 *   "lastUpdate": "2023-02-14"
 * }
 */
@Data
public class BillingAccount {
    private Long id;

    private String name; /* firstname + lastname */

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date lastUpdate;

}


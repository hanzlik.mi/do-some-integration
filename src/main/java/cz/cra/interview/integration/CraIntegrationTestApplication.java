package cz.cra.interview.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CraIntegrationTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CraIntegrationTestApplication.class, args);
	}

}
